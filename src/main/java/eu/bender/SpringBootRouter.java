package eu.bender;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class SpringBootRouter extends RouteBuilder {

    @Override
    public void configure() {
        from("servlet:/hello")
                .setBody(constant("Hello world!"));
        
        from("servlet:/test")
                .setHeader("content-type", constant("application/json"))
                .setBody(simple("{\"test\":\"value\"}"));
    }
}
